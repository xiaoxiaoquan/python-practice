
// 配置 路由的地方

// 引入 vue
import Vue from 'vue'

// 引入 vue-router
import VueRouter from 'vue-router'


// 引入 路由组件
import School from '../pages/School.vue'
import Student from '../pages/Student.vue'

// 使用插件
Vue.use(VueRouter)

// 1. 先把 VueRouter 原型对象的 push 方法 保存一份 

// (我们重写的方法还是使用 官方提供的 push 方法)

let originPush = VueRouter.prototype.push;

// 重写 push 方法 

// 第一个参数 : 告诉 push 方法 , 往哪里跳转 (传递那些参数)
VueRouter.prototype.push = function (location, resole, reject) {
    console.log(this)
    if (resole && reject) {

        // 这里使用 call 调用 原来的 push 方法 , 并把参数传递
        originPush.call(this, location, resole, reject);
    } else {
        
        // 此时 没有传递 回调函数 手动传递两个
        originPush.call(this,location,()=>{},()=>{})
    }
}

// 补充 :  call || apply 区别

// 相同点 : 都可以 调用函数一次 , 都可以篡改函数的上下文一次

// 不同点 : call 与 apply 传递参数 , call 传递参数用逗号 隔开 , 
// apply 方法 执行 , 传递数组

let originReplace = VueRouter.prototype.replace;

// 第二个参数成功的回调 , 第三个参数 失败的回调
VueRouter.prototype.replace = function(location,resole,reject){
    if(resole && reject){
        originReplace.call(this,location,resole,reject)
    }else {
        originPush.call(this,location,()=>{},()=>{})
    }
}



// 配置路由

export default new VueRouter({
    routes: [
        {
            // 这里我们传递 params 参数 需要占位 所以添加 /:schoolName
            path: '/school/:parameter?',
            // path:'/school',
            component: School,
            name: 'school',
            meta: { isShow: false },



            // 布尔值写法 
            // props: true,

            // 对象写法
            // props: { parameter: "你好" , a: 1, b: 2 },

            // 函数写法 : 
            props: ($router) => {
                return {
                    schoolName: $router.query.schoolName,
                    parameter: $router.parameter
                };
            },


        },

        {
            path: '/student',
            component: Student,
            meta: { isShow: true },
            name: 'student'
        },
        // 重定向 , 在项目跑起来的时候, 访问 / , 立刻让他重定向到 Student 组件
        {
            path: "*",
            redirect: "/student"
        }

    ]
})