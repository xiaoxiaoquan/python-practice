vue 路由组件









# 1. 搭建路由组件



解释一下啥是路由 : 路由就是 一组映射关系 (key - value) , key 为路径 , value 可能是 function  或 component.



在 vue2  中想要使用 路由 需要安装一个插件 vue-router .



## 1.1 安装 vue-router

![image-20230418193029866](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418193029866.png)



<font color = ddii size=4>创建 对应 目录 与 组件</font>



![image-20230418193440451](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418193440451.png)



## 1.2 配置路由



<font color = ooddoo size=4>在项目中 配置的路由 一般存放在 router 文件夹中</font>



![image-20230418194546062](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418194546062.png)



# 2. 使用路由



图一 : 

![image-20230418201424603](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418201424603.png)



图二 :



![image-20230418201557198](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418201557198.png)



# 3. 路由的跳转





<font color = ddii size=4>上面 是 手动 通过 url 来进行路由的切换 ，这肯定是不行的 , 用户看不太懂这些，下面我们就来看看路由的两种跳转模式</font>





## 3.1 声明式导航 



第一种路由跳转 : 声明式导航 通过 router-link  标签 进行路由跳转 



![image-20230418202930163](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418202930163.png)







## 3.2 编程式导航



第一种路由跳转 : 编程式导航 使用 push | replace 进行路由跳转.



<font color = ddii size=4>关于声明式导航 能做的 ，编程式导航都能做 ， 关于 编程式导航 除了可以进行路由跳转 ，还可以做一些其他的业务逻辑</font>



![image-20230418210106669](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418210106669.png)



# 4. 路由传参



<font color = ddii size=4>关于 路由传参 ， 参数有两种 </font>

1.  params 参数 :  属于路径当中的一部分 , 需要注意 ，在配置路由的时候 ，需要占位
2. query 参数 : 不属于 路径当中的一部分 ，类似 ajax 中的 queryString `/student?k=v&kv=`  不需要传参





## 4.1 路由传递参数 : 字符串形式

 



图一 : 

![image-20230418221739170](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418221739170.png)





图二 ： 

![image-20230418221801564](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418221801564.png)



<font color = ooddoo size=5>使用模板字符串</font>



![image-20230418221839880](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418221839880.png)



## 4.2 路由传递参数 : 对象写法



图一 :

![image-20230418225504854](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418225504854.png)



图二 : 

![image-20230418225527700](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418225527700.png)



图三 :

![image-20230418225555478](C:\Users\A\AppData\Roaming\Typora\typora-user-images\image-20230418225555478.png)



# 5. meta 使用

