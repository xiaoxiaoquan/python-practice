
// 配置 路由的地方

// 引入 vue
import Vue from 'vue'

// 引入 vue-router
import VueRouter from 'vue-router'


// 引入 路由组件
import School from '../pages/School.vue'
import Student from '../pages/Student.vue'

// 使用插件
Vue.use(VueRouter)

// 配置路由

export default new VueRouter({
    routes: [
        {
            // 这里我们传递 params 参数 需要占位 所以添加 /:schoolName
            path: '/school/:schoolName',

            // path:'/school',
            component: School,
            name: 'school'
        },
        {
            path: '/student',
            component: Student
        },
        // 重定向 , 在项目跑起来的时候, 访问 / , 立刻让他重定向到 Student 组件
        {
            path: "*",
            redirect: "/student"
        }

    ]
})